fn main(){
    v1();
}

#[allow(warnings)]
fn v1(){
    use tomcat::*;
    let res = get_blocking("http://www.tesla.com").expect("get error");
    assert_eq!(200,res.status);
    println!("{}",res.remote_addr);
}

