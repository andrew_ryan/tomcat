# Send HTTP/HTTPS requests.
[![Crates.io](https://img.shields.io/crates/v/tomcat.svg)](https://crates.io/crates/tomcat)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/tomcat)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/tomcat/-/raw/master/LICENSE)

---
```
cargo add tomcat
```
```
tokio = { version = "1.21.2", features = ["full"] }
tomcat = "0.1.1" 
```
## Examples
---
## non-blocking get
```rust
#[tokio::main]
async fn main(){
    use tomcat::*;
    if let Ok(res) = get("https://www.spacex.com").await{
        assert_eq!(200,res.status);
        assert_eq!(r#"{"content-type": "text/html; charset=utf-8", "vary": "Accept-Encoding", "date": "Sun, 09 Oct 2022 18:49:44 GMT", "connection": "keep-alive", "keep-alive": "timeout=5", "transfer-encoding": "chunked"}"#,format!("{:?}",res.headers));
        println!("{}",res.text);
        println!("{}",res.text_with_charset);
        println!("{}",res.url);
        println!("{}",res.remote_addr);
        println!("{:?}",res.version);
    }
    }
    pub fn look_ip(url: &str){
        use tomcat;
        use tokio::runtime::Runtime;
        let mut io_loop = Runtime::new().unwrap();
        
        let res = tomcat::get(url);
        let mut response = io_loop.block_on(res).unwrap();

        let addr = response.remote_addr.ip();
        print!("{:?}",addr);
    }
```

## blocking get
```rust
fn main(){
    use tomcat::*;
    if let Ok(res) = get_blocking("https://www.spacex.com"){
        assert_eq!(200,res.status);
        assert_eq!(r#"{"content-type": "text/html; charset=utf-8", "vary": "Accept-Encoding", "date": "Sun, 09 Oct 2022 18:49:44 GMT", "connection": "keep-alive", "keep-alive": "timeout=5", "transfer-encoding": "chunked"}"#,format!("{:?}",res.headers));
        println!("{}",res.text);
        println!("{}",res.text_with_charset);
        println!("{}",res.url);
        println!("{}",res.remote_addr);
        println!("{:?}",res.version);
    }
}

```

## non-blocking post
```rust
#[tokio::main]
async fn main() {
    openai().await.unwrap();
}
async fn openai()-> Result<(), Box<dyn std::error::Error + Send + Sync>>{
    use std::env::{self, args};
    use reqwest::Body;
    use serde_derive::Serialize;
    use serde_derive::Deserialize;
    use http::header;
    #[derive(Serialize, Debug)]
    struct OpenAIRequest {
        model: String,
        prompt: String,
        max_tokens: u32,
        stop: String,
    }
    #[derive(Deserialize, Debug)]
    struct OpenAIChoices {
        text: String,
    }

    #[derive(Deserialize, Debug)]
    struct OpenAIResponse {
        choices: Vec<OpenAIChoices>,
    }

    let api_key = match env::var("OPENAI_KEY") {
        Ok(key) => key,
        Err(_) => {
            println!("Error: please create an environment variable OPENAI_KEY");
            std::process::exit(1);
        }
    };
    
    let uri = "https://api.openai.com/v1/completions";

    let model = String::from("text-davinci-002");
    let stop = String::from("Text");

    let default_prompt =
        "Given text, return 1 bash command. Text:list contents of a directory. Command:ls";
    let mut user_input = String::new();

    let mut arguments: Vec<String> = args().collect();
    arguments.remove(0);

    if arguments.is_empty() {
        println!("Welcome to Rusty! Enter an argument to get started.");
        std::process::exit(1);
    }

    for x in arguments {
        user_input.push(' ');
        user_input.push_str(&x);
    }

    let auth_header_val = format!("Bearer {}", api_key);

    let openai_request = OpenAIRequest {
        model,
        prompt: format!("{} Text:{}. Command:", default_prompt, user_input),
        max_tokens: 64,
        stop,
    };

    let body = Body::from(serde_json::to_vec(&openai_request)?);

    //
    if let Ok(req) = tomcat::post("https://api.openai.com/v1/completions").await {
        let res = req
        .header(header::CONTENT_TYPE, "application/json")
        .header("Authorization", &auth_header_val)
        .body(body).send().await.unwrap();
        let text = res.text().await.unwrap();

        let json: OpenAIResponse = match serde_json::from_str(&text){
            Ok(response) => response,
            Err(_) => {
                println!("Error calling OpenAI. Check environment variable OPENAI_KEY");
                std::process::exit(1);
            }
        };
    
        println!(
            "{}",
            json.choices[0]
                .text
                .split("\n")
                .map(|s| s.trim())
                .filter(|s| s.len() > 0)
                .collect::<Vec<_>>()
                .join("\n")
        );
        
    }

    Ok(())
}
``` 

## blocking post
```rust
fn main(){
    openai_blocking().unwrap();
}
fn openai_blocking()-> Result<(), Box<dyn std::error::Error + Send + Sync>>{
    use std::env::{self, args};
    use reqwest::blocking::Body;
    use serde_derive::Serialize;
    use serde_derive::Deserialize;
    use http::header;
    #[derive(Serialize, Debug)]
    struct OpenAIRequest {
        model: String,
        prompt: String,
        max_tokens: u32,
        stop: String,
    }
    #[derive(Deserialize, Debug)]
    struct OpenAIChoices {
        text: String,
    }

    #[derive(Deserialize, Debug)]
    struct OpenAIResponse {
        choices: Vec<OpenAIChoices>,
    }

    let api_key = match env::var("OPENAI_KEY") {
        Ok(key) => key,
        Err(_) => {
            println!("Error: please create an environment variable OPENAI_KEY");
            std::process::exit(1);
        }
    };
    
    let uri = "https://api.openai.com/v1/completions";

    let model = String::from("text-davinci-002");
    let stop = String::from("Text");

    let default_prompt =
        "Given text, return 1 bash command. Text:list contents of a directory. Command:ls";
    let mut user_input = String::new();

    let mut arguments: Vec<String> = args().collect();
    arguments.remove(0);

    if arguments.is_empty() {
        println!("Welcome to Rusty! Enter an argument to get started.");
        std::process::exit(1);
    }

    for x in arguments {
        user_input.push(' ');
        user_input.push_str(&x);
    }

    let auth_header_val = format!("Bearer {}", api_key);

    let openai_request = OpenAIRequest {
        model,
        prompt: format!("{} Text:{}. Command:", default_prompt, user_input),
        max_tokens: 64,
        stop,
    };

    let body = Body::from(serde_json::to_vec(&openai_request)?);

    if let Ok(req) = tomcat::post_blocking("https://api.openai.com/v1/completions"){
        let res = req
        .header(header::CONTENT_TYPE, "application/json")
        .header("Authorization", &auth_header_val)
        .body(body).send().unwrap();
        let text = res.text().unwrap();

        let json: OpenAIResponse = match serde_json::from_str(&text){
            Ok(response) => response,
            Err(_) => {
                println!("Error calling OpenAI. Check environment variable OPENAI_KEY");
                std::process::exit(1);
            }
        };
    
        println!(
            "{}",
            json.choices[0]
                .text
                .split("\n")
                .map(|s| s.trim())
                .filter(|s| s.len() > 0)
                .collect::<Vec<_>>()
                .join("\n")
        );
        
    }

    Ok(())
}
```